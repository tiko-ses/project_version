from django import template
from django.utils.safestring import mark_safe
from dateutil import parser
import subprocess
import os
import sys
import json
import logging

logger = logging.getLogger('dev')

register = template.Library()

try:
    app_version = {
        'main_project': {
            'DATE': parser.parse(subprocess.check_output(
                ['git', 'log', '-1', '--format=%cd']
            )).strftime('%d-%m-%y'),
            'HASH': subprocess.check_output(
                ['git', 'log', '-1', '--format=%H'],
                universal_newlines=True
            ).strip(),  # strip output without \n
        },
    }
except:
    logger.debug("No project version info found.")
    app_version = {}

try:
    for submodule_name in os.listdir('.git/modules'):
        if submodule_name.startswith("."):
            continue
        app_version[submodule_name] = {
            'DATE': parser.parse(subprocess.check_output(
                ['git', '-C', submodule_name, 'log', '-1', '--format=%cd']
            )).strftime('%d-%m-%y'),
            'HASH': subprocess.check_output(
                ['git', '-C', submodule_name, 'log', '-1', '--format=%H'],
                universal_newlines=True
            ).strip(),  # strip output without \n
        }
except OSError:
    logger.debug("No sub-modules found.")

app_version['python_version'] = sys.version

logger.debug('VERSION {}'.format(app_version))


@register.simple_tag
def version(as_dict=False):
    return app_version if as_dict else mark_safe(json.dumps(app_version))
