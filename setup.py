from setuptools import setup

setup(name='project_version',
      version='0.0.3',
      description='Template tag package to create a version object',
      url='https://bitbucket.org/tiko-ses/project_version',
      author='Tiko - Swisscom Energy Solutions AG',
      author_email='marcelo.conceicao@tiko.ch',
      license='MIT',
      packages=['project_version'],
      install_requires=[
          'django',
          'python-dateutil',
      ],
      zip_safe=False)
